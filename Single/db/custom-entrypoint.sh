#!/bin/bash
set -e

docker-entrypoint.sh

if [ "$1" = 'help' ]; then
    echo 'Commands:'
    echo '   help - show this message'
    echo '   init - init database'
    exec echo 'Ok'
fi
if [ "$1" = 'init' ]; then
    echo 'Init database'
    exec echo 'Ok'
fi
echo 'Start database server'

exec "$@"

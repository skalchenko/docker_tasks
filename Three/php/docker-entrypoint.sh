#!/bin/bash

let www_uid=1005
let www_gid=1005

if [ "$PHP_GID" != "" ]; then
    let www_gid=$PHP_GID
fi

if [ "$PHP_UID" != "" ]; then
    let www_uid=$PHP_UID
fi

addgroup --quiet --system --gid "${www_gid}" php-group
adduser  --quiet --system --no-create-home --disabled-password --disabled-login --uid "$www_uid" --gid "$www_gid" php-user

exec "$@"
